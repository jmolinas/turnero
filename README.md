# TURNERO 

## Puesta a punto 

Instalar Python y Pip

[Python] (https://www.python.org/downloads/)

[pip] (https://pip.pypa.io/en/stable/)

Luego instalar el virtualenv

```bash
pip install virtualenv
```

Luego crear e iniciar el entorno virtual

```bash
python -m venv turnero-env
```

```bash
.\turnero-env\Scripts\activate
```

Luego instalar las dependencias 

```bash
pip install -r requirements.txt
```

## Ejecucion del proyecto

Para ello debemos siempre estar dentro de nuestro entorno virtual y ejecutamos la siguiente línea.

```bash
python .\manage.py runserver
```

## Para detener la ejecucio

Ctrl + c

## Para salir del entorno virtual

```bash
deactivate
```
